# Use the default build backend, but with access to local __version__.py
import __version__
from setuptools.build_meta import (
    get_requires_for_build_wheel,
    get_requires_for_build_sdist,
    prepare_metadata_for_build_wheel,
    build_wheel,
    build_sdist,
    build_editable,
)

__all__ = [
    "__version__",
    "get_requires_for_build_wheel",
    "get_requires_for_build_sdist",
    "prepare_metadata_for_build_wheel",
    "build_wheel",
    "build_sdist",
    "build_editable",
]
